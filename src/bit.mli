(** Represents a bit value, either [0] or [1] *)
type t

(** Represents the bit set to [1] *)
val one : t

(** Represents the bit set to [0] *)
val zero : t

(** [is_one x] returns [true] if [x] is set to [1], else [false] *)
val is_one : t -> bool

(** [is_zero x] returns [true] if [x] is set to [0] else [false] *)
val is_zero : t -> bool

(** [eq x y] returns [true] if [x] and [y] handles the same value *)
val eq : t -> t -> bool

(** [to_int x] returns [1] if [x] is the bit set to [1] else returns [0] *)
val to_int : t -> int

(** [of_bool x] returns {!one} if [x = true] else returns {!zero} *)
val of_bool : bool -> t

(** [to_bool x] returns [true] if [x = one] else returns [false] *)
val to_bool : t -> bool

(** "not" operator *)
val not : t -> t

(** "and" operator *)
val ( && ) : t -> t -> t

(** "or" operator *)
val ( || ) : t -> t -> t

(** "xor" operator *)
val ( <> ) : t -> t -> t

val le_bit_of_bytes_exn : Bytes.t -> int -> t

val le_bits_of_bytes_exn : Bytes.t -> t list

val map : (t -> 'a) -> t list -> 'a list

val mapi : (int -> t -> 'a) -> t list -> 'a list

val iter : (t -> unit) -> t list -> unit

val iteri : (int -> t -> unit) -> t list -> unit
