type t = bool

let one = true

let zero = false

let is_one b = b && true

let is_zero b = not (is_one b)

let eq b1 b2 = b1 = b2

let to_int b = if b then 1 else 0

let of_bool b = b

let to_bool b = b

let le_bit_of_bytes_exn b idx =
  let l = Bytes.length b in
  if idx < 0 || idx >= l * 8 then
    invalid_arg
      (Printf.sprintf
         "get_bit_of_bytes: out of bounds. Given idx %d, bytes of length %d"
         idx
         l)
  else
    let idx_in_bytes_list = idx / 8 in
    let bit_of_bytes = idx mod 8 in
    let corresponding_byte = int_of_char @@ Bytes.get b idx_in_bytes_list in
    (corresponding_byte lsr (8 - 1 - bit_of_bytes)) mod 2 = 1

let le_bits_of_bytes_exn b =
  (* FIXME: not optimized at all, just lazy *)
  List.init (Bytes.length b * 8) (fun i -> le_bit_of_bytes_exn b i)

let map f bs = List.map f bs

let mapi f bs = List.mapi f bs

let iter f bs = List.iter f bs

let iteri f bs = List.iteri f bs

let not = not

let ( && ) = ( && )

let ( || ) = ( || )

let ( <> ) = ( <> )
