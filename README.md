Library to manipulate bits in OCaml
============================================

## Motivation

Manipulating bits is an operation required in different applications, in
particular in cryptography.
Different libraries exist in OCaml to manipulate bits like
[bitv](https://github.com/backtracking/bitv/) and
[funfields](https://github.com/fccm/ocaml-funfields) but either it is not
maintained anymore or the interface is not intuitive.
In particular, none of these libraries allows the user to build a bit sequence
from a byte sequence and give the choice of the endianess.


## This work

WIP

## Install

You need to pin the repository for the moment:

```
opam pin add bit.dev git+https://gitlab.com/dannywillems/ocaml-bit.git\#master
opam install bit
```
