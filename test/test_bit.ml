module Bit = R1cs__Bit

let test_get_bit_of_bytes () =
  let test_vectors =
    [ ((Bytes.init 1 (fun _i -> char_of_int 0), 0), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 1), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 2), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 3), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 4), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 5), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 6), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 0), 7), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 0), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 1), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 2), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 3), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 4), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 5), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 6), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 255), 7), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 0), Bit.one);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 1), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 2), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 3), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 4), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 5), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 6), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int 128), 7), Bit.zero);
      (* ((Bytes.init 1 (fun _i -> char_of_int 128), 2), Bit.zero); *)
      ((Bytes.init 1 (fun _i -> char_of_int 64), 0), Bit.zero);
      ((Bytes.init 1 (fun _i -> char_of_int (64 + 128)), 0), Bit.one) ]
  in
  List.iter
    (fun ((bytes, idx), expected_value) ->
      let res = Bit.le_bit_of_bytes_exn bytes idx in
      if not (Bit.eq res expected_value) then
        Alcotest.failf
          "Expected value %d, got %d"
          (Bit.to_int expected_value)
          (Bit.to_int res))
    test_vectors

let test_get_bits_of_bytes () =
  let test_vectors =
    [ ( Bytes.init 1 (fun _i -> char_of_int 0),
        [ Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero ] );
      ( Bytes.init 1 (fun _i -> char_of_int 128),
        [ Bit.one;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero ] );
      ( Bytes.init 1 (fun _i -> char_of_int (128 + 1)),
        [ Bit.one;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.one ] );
      ( Bytes.init 1 (fun _i -> char_of_int (128 + 2)),
        [ Bit.one;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.zero;
          Bit.one;
          Bit.zero ] ) ]
  in
  List.iter
    (fun (bytes, expected_value) ->
      let res = Bit.le_bits_of_bytes_exn bytes in
      assert (res = expected_value))
    test_vectors

let () =
  let open Alcotest in
  run
    "Bit module"
    [ ( "Bit tests",
        [ test_case "vectors get_bit_of_bytes" `Quick test_get_bit_of_bytes;
          test_case "vectors get_bits_of_bytes" `Quick test_get_bits_of_bytes ]
      ) ]
